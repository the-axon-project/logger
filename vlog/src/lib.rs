// Copyright (C) 2019  Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ---------------------------------------------------------
//
// # Lint Levels
//
// Lints are divided into four levels:
//
// 1. allow
// 2. warn
// 3. deny
// 4. forbid
//
// To override the lints set here, use:
//
// ```sh
// $ RUSTFLAGS="--cap-lints warn" cargo build
// ```
//
// ---------------------------------------------------------
//
// # Clippy lints
//
// code that is just outright wrong or very very useless
#![forbid(clippy::correctness)]
// code that should be written in a more idiomatic way
#![forbid(clippy::style)]
// code that does something simple but in a complex way
#![forbid(clippy::complexity)]
// code that can be written in a faster way
#![forbid(clippy::perf)]
// lints which are rather strict
#![forbid(clippy::pedantic)]
// new lints that aren't quite ready yet
#![forbid(clippy::nursery)]
// checks against the cargo manifest
#![forbid(clippy::cargo)]
// ---------------------------------------------------------
//
// # Lint Groups
//
// Lint groups toggle several lints through one name.
// The following lint groups are available:
//
// - nonstandard-style
// - warnings
// - rust-2018-idioms
// - unused
// - future-incompatible
//
// Violation of standard naming conventions
#![forbid(nonstandard_style)]
// all lints that would be issuing warnings
#![forbid(warnings)]
// Lints to nudge you toward idiomatic features of Rust 2018
#![forbid(rust_2018_idioms)]
// These lints detect things being declared but not used
#![forbid(unused)]
// Lints that detect code that has future-compatibility problems
#![forbid(future_incompatible)]
// ---------------------------------------------------------
//
// # Other lints
//
// This lint detects the use of hidden lifetime parameters.
#![forbid(elided_lifetimes_in_paths)]
// This lint detects potentially-forgotten implementations of Copy.
#![forbid(missing_copy_implementations)]
// This lint detects missing implementations of fmt::Debug.
#![forbid(missing_debug_implementations)]
// This lint detects missing documentation for public items.
#![forbid(missing_docs)]
// This lint detects lifetimes that are only used once.
#![forbid(single_use_lifetimes)]
// This lint detects trivial casts which could be removed.
#![forbid(trivial_casts)]
// This lint detects trivial casts of numeric types which could be removed.
#![forbid(trivial_numeric_casts)]
// This lint catches usage of unsafe code.
#![forbid(unsafe_code)]
// This lint catches unnecessary braces around an imported item.
#![forbid(unused_import_braces)]
// This lint detects unnecessarily qualified names.
#![forbid(unused_qualifications)]
// This lint checks for the unused result of an expression in a statement.
#![forbid(unused_results)]
// This lint detects enums with widely varying variant sizes.
#![forbid(variant_size_differences)]
// ---------------------------------------------------------
//
// # Lint overrides
//
// Needed for deriving traits
#![deny(unused_qualifications)]

//! # The `log` crate.
//!
//! The `log` crate provides structured contextual logging.
//!
//! Logging is done using the `event!` macro.

// Do not use the standard library.
#![no_std]
// Allow constant functions
#![feature(const_fn)]
// Enable configuration for documentation
#![feature(doc_cfg)]

// XXX: See https://github.com/rust-lang/rust/issues/54010
#[doc(cfg(test))]
extern crate std;

// Heap allocations
extern crate alloc;

// Heap allocated Strings
use alloc::string::String;

// Boxed pointers for recursive types
use alloc::boxed::Box;

/// # Context
///
/// The context within which logging occurs.
#[derive(Debug, Clone)]
#[must_use]
pub struct Context<S: Sink> {
    /// The name of the context.
    pub name: String,

    /// The sink that consumes the log messages.
    ///
    /// The sink can either be a Sink or it could be the parent context's Sink.
    pub sink: Either<S, Box<Self>>,
}

/// The enum `Either` with variants `Left` and `Right` is a general purpose
/// sum type with two cases.
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[must_use]
pub enum Either<L, R> {
    /// Left value
    Left(L),

    /// Right value
    Right(R),
}

impl<S: Sink> Context<S> {
    /// Create the root context.
    ///
    /// This function should only be used to create the root context.
    /// See `Context::child` for creating a child context.
    #[inline]
    pub const fn root(
        name: String,
        sink: S,
    ) -> Self {
        Self {
            name,
            sink: Either::Left(sink),
        }
    }

    /// Create a child context.
    #[inline]
    pub fn child(
        self,
        name: String,
    ) -> Self {
        Self {
            name,
            sink: Either::Right(Box::new(self)),
        }
    }
}

impl Default for Context<NOPSink> {
    #[inline]
    fn default() -> Self {
        Self {
            name: String::from("NOP"),
            sink: Either::Left(NOP_SINK_INSTANCE),
        }
    }
}

/// # Event
///
/// A log event.
#[derive(Debug, Clone)]
#[must_use]
pub struct Event<'a, S: Sink> {
    /// The context within which the event occurred.
    pub context: &'a Context<S>,

    /// The metadata associated with the event.
    pub metadata: Metadata,

    /// The logging level of the event.
    pub level: Level,

    /// The message associated with the event.
    pub message: &'a str,
}

/// # Metadata
///
/// Metadata associated with a log `Event`.
#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[must_use]
pub struct Metadata {
    /// The module path in which an `Event` occurred.
    pub module_path: &'static str,

    /// The file path in which an `Event` occurred.
    pub file: &'static str,

    /// The line number in which an `Event` occurred.
    pub line: u32,

    /// The column number in which an `Event` occurred.
    pub column: u32,
}

/// # Level
///
/// The logging level associated with an `Event`.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[must_use]
pub enum Level {
    /// A catastrophic `Event` that is guaranteed to cause the application to
    /// fail.
    Fatal,

    /// An `Event` that is likely to cause the application to fail.
    Error,

    /// An `Event` to signal that something may be wrong.
    Warning,

    /// An `Event` to relay information that is useful for monitoring.
    Information,

    /// An `Event` to relay information that is useful for debugging.
    Debug,

    /// An `Event` to relay information that is useful for tracing.
    Trace,
}

/// Record the value with the root sink
pub fn root_sink_record<S: Sink>(
    context: &Context<S>,
    value: &Event<'_, S>,
) {
    match &context.sink {
        Either::Left(sink) => Sink::record(sink, value),
        Either::Right(parent) => root_sink_record(parent, value),
    }
}

/// # Sink
///
/// A consumer of log messages.
pub trait Sink: Send + Sync {
    // XXX: UnwindSafe is not in libcore
    // https://github.com/rust-lang/rust/issues/64086
    // It would be useful to have Sink require UnwindSafe

    /// Record a single event.
    fn record<S: Sink>(
        &self,
        value: &Event<'_, S>,
    );
}

/// NOP.
///
/// A special sink that ignores events.
#[derive(Debug, Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]

pub struct NOPSink {}

impl Sink for NOPSink {
    #[inline]
    fn record<S: Sink>(
        &self,
        _value: &Event<'_, S>,
    ) {
    }
}

/// An instance of `NOPSink`.
pub const NOP_SINK_INSTANCE: NOPSink = NOPSink {};

/// # Filter level
///
/// Calculates the maximum allowed logging level.
/// Returns None if logging is disabled.
/// All logging levels less than or equal to the filter level are logged.
#[must_use]
#[inline]
pub fn filter_level() -> Option<Level> {
    if cfg!(debug_assertions) {
        if cfg!(feature = "max_level_trace") {
            Some(Level::Trace)
        } else if cfg!(feature = "max_level_debug") {
            Some(Level::Debug)
        } else if cfg!(feature = "max_level_info") {
            Some(Level::Information)
        } else if cfg!(feature = "max_level_warning") {
            Some(Level::Warning)
        } else if cfg!(feature = "max_level_error") {
            Some(Level::Error)
        } else if cfg!(feature = "max_level_fatal") {
            Some(Level::Fatal)
        } else {
            None
        }
    } else if cfg!(feature = "release_max_level_trace") {
        Some(Level::Trace)
    } else if cfg!(feature = "release_max_level_debug") {
        Some(Level::Debug)
    } else if cfg!(feature = "release_max_level_info") {
        Some(Level::Information)
    } else if cfg!(feature = "release_max_level_warning") {
        Some(Level::Warning)
    } else if cfg!(feature = "release_max_level_error") {
        Some(Level::Error)
    } else if cfg!(feature = "release_max_level_fatal") {
        Some(Level::Fatal)
    } else {
        None
    }
}

/// Event macro
///
/// Usage: event!(&context, Level::Trace, message = "Hello, World!");
#[macro_export]
macro_rules! event(
    ($context:expr, $level:expr, message = $message:expr) => {
        if $crate::filter_level().is_some()
                && $level <= $crate::filter_level().unwrap() {
            let metadata = $crate::Metadata {
	        module_path: module_path!(),
	        file: file!(),
	        line: line!(),
	        column: column!(),
            };
            let event = $crate::Event {
                context: $context,
                metadata,
                level: $level,
                message: $message,
            };
            $crate::root_sink_record($context, &event);
        }
    };
);
