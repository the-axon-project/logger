// Copyright (C) 2019  Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// # Lint Levels
//
// Lints are divided into four levels:
//
// 1. allow
// 2. warn
// 3. deny
// 4. forbid
//
// To override the lints set here, use:
//
// ```sh
// $ RUSTFLAGS="--cap-lints warn" cargo build
// ```

// code that is just outright wrong or very very useless
#![forbid(clippy::correctness)]
// code that should be written in a more idiomatic way
#![forbid(clippy::style)]
// code that does something simple but in a complex way
#![forbid(clippy::complexity)]
// code that can be written in a faster way
#![forbid(clippy::perf)]
// lints which are rather strict
#![forbid(clippy::pedantic)]
// new lints that aren't quite ready yet
#![forbid(clippy::nursery)]
// checks against the cargo manifest
#![forbid(clippy::cargo)]
// # Lint Groups
//
// Lint groups toggle several lints through one name.
// The following lint groups are available:
//
// - nonstandard-style
// - warnings
// - rust-2018-idioms
// - unused
// - future-incompatible

// Violation of standard naming conventions
#![forbid(nonstandard_style)]
// all lints that would be issuing warnings
#![forbid(warnings)]
// Lints to nudge you toward idiomatic features of Rust 2018
#![forbid(rust_2018_idioms)]
// These lints detect things being declared but not used
#![forbid(unused)]
// Lints that detect code that has future-compatibility problems
#![forbid(future_incompatible)]
// # Other lints

// This lint detects the use of hidden lifetime parameters.
#![forbid(elided_lifetimes_in_paths)]
// This lint detects potentially-forgotten implementations of Copy.
#![forbid(missing_copy_implementations)]
// This lint detects missing implementations of fmt::Debug.
#![forbid(missing_debug_implementations)]
// This lint detects missing documentation for public items.
#![forbid(missing_docs)]
// This lint detects lifetimes that are only used once.
#![forbid(single_use_lifetimes)]
// This lint detects trivial casts which could be removed.
#![forbid(trivial_casts)]
// This lint detects trivial casts of numeric types which could be removed.
#![forbid(trivial_numeric_casts)]
// This lint catches usage of unsafe code.
#![forbid(unsafe_code)]
// This lint catches unnecessary braces around an imported item.
#![forbid(unused_import_braces)]
// This lint detects unnecessarily qualified names.
#![forbid(unused_qualifications)]
// This lint checks for the unused result of an expression in a statement.
#![forbid(unused_results)]
// This lint detects enums with widely varying variant sizes.
#![forbid(variant_size_differences)]

//! Example program for logging
//!
//! Run using:
//!
//! ```sh
//! $ cargo run --example log --features log/max_level_trace
//! ```

use vlog::{
    event,
    Context,
    Level,
};
use vlog_writer::Writer;

fn main() {
    let writer = Writer::new(std::io::stderr());
    let context = Context::root(String::from("root"), writer);

    event!(&context, Level::Fatal, message = "This is a fatar error message.");

    event!(&context, Level::Error, message = "This is an error message.");

    event!(&context, Level::Warning, message = "This is a warning.");

    event!(&context, Level::Information, message = "This is information.");

    event!(&context, Level::Debug, message = "This is a debug message.");

    event!(&context, Level::Trace, message = "This is a trace.");
}
