// Copyright (C) 2019  Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

// ---------------------------------------------------------
//
// # Lint Levels
//
// Lints are divided into four levels:
//
// 1. allow
// 2. warn
// 3. deny
// 4. forbid
//
// To override the lints set here, use:
//
// ```sh
// $ RUSTFLAGS="--cap-lints warn" cargo build
// ```
//
// ---------------------------------------------------------
//
// # Clippy lints
//
// code that is just outright wrong or very very useless
#![forbid(clippy::correctness)]
// code that should be written in a more idiomatic way
#![forbid(clippy::style)]
// code that does something simple but in a complex way
#![forbid(clippy::complexity)]
// code that can be written in a faster way
#![forbid(clippy::perf)]
// lints which are rather strict
#![forbid(clippy::pedantic)]
// new lints that aren't quite ready yet
#![forbid(clippy::nursery)]
// checks against the cargo manifest
#![forbid(clippy::cargo)]
// ---------------------------------------------------------
//
// # Lint Groups
//
// Lint groups toggle several lints through one name.
// The following lint groups are available:
//
// - nonstandard-style
// - warnings
// - rust-2018-idioms
// - unused
// - future-incompatible
//
// Violation of standard naming conventions
#![forbid(nonstandard_style)]
// all lints that would be issuing warnings
#![forbid(warnings)]
// Lints to nudge you toward idiomatic features of Rust 2018
#![forbid(rust_2018_idioms)]
// These lints detect things being declared but not used
#![forbid(unused)]
// Lints that detect code that has future-compatibility problems
#![forbid(future_incompatible)]
// ---------------------------------------------------------
//
// # Other lints
//
// This lint detects the use of hidden lifetime parameters.
#![forbid(elided_lifetimes_in_paths)]
// This lint detects potentially-forgotten implementations of Copy.
#![forbid(missing_copy_implementations)]
// This lint detects missing implementations of fmt::Debug.
#![forbid(missing_debug_implementations)]
// This lint detects missing documentation for public items.
#![forbid(missing_docs)]
// This lint detects lifetimes that are only used once.
#![forbid(single_use_lifetimes)]
// This lint detects trivial casts which could be removed.
#![forbid(trivial_casts)]
// This lint detects trivial casts of numeric types which could be removed.
#![forbid(trivial_numeric_casts)]
// This lint catches usage of unsafe code.
#![forbid(unsafe_code)]
// This lint catches unnecessary braces around an imported item.
#![forbid(unused_import_braces)]
// This lint detects unnecessarily qualified names.
#![forbid(unused_qualifications)]
// This lint checks for the unused result of an expression in a statement.
#![forbid(unused_results)]
// This lint detects enums with widely varying variant sizes.
#![forbid(variant_size_differences)]
// ---------------------------------------------------------
//
// # Lint overrides
//
// Needed by the `?` operator
#![deny(unreachable_code)]
// Needed for deriving certain traits
#![deny(unused_qualifications)]

//! # Log Writer
//!
//! A log sink for any Writer.

mod date;
mod datetime;
mod time;

use datetime::DateTime;
use std::{
    io::{
        Error,
        ErrorKind,
        Write,
    },
    sync::{
        Arc,
        Mutex,
    },
};
use vlog::{
    Event,
    Level,
    Sink,
};

/// # Writer
///
/// A newtype wrapper which turns an arbitrary writer into a thread-safe writer.
#[derive(Debug, Clone, Default)]
#[must_use]
#[repr(transparent)]
pub struct Writer<W: Write>(Arc<Mutex<W>>);

impl<W: Write> Writer<W> {
    /// Create a thread-safe writer
    #[inline]
    pub fn new(writer: W) -> Self {
        Self(Arc::new(Mutex::new(writer)))
    }
}

impl<W: Write> Write for Writer<W> {
    #[inline]
    fn write(
        &mut self,
        buf: &[u8],
    ) -> Result<usize, Error> {
        self.0
            .lock()
            .map_err(|err| Error::new(ErrorKind::WouldBlock, err.to_string()))?
            .write(buf)
    }

    #[inline]
    fn flush(&mut self) -> Result<(), Error> {
        self.0
            .lock()
            .map_err(|err| Error::new(ErrorKind::WouldBlock, err.to_string()))?
            .flush()
    }
}

/// ANSI escape sequence for making text magenta.
const MAGENTA: &str = "\x1b[35m";

/// ANSI escape sequence for making text cyan.
const CYAN: &str = "\x1b[36m";

/// ANSI escape sequence for resetting text attributes.
const RESET: &str = "\x1b[0m";

/// Level string for fatal
const FATAL: &str = "\x1b[1;4;31m...FATAL...\x1b[0m"; // RED BOLD UNDERSCORE

/// Level string for error
const ERROR: &str = "\x1b[1;31m...ERROR...\x1b[0m"; // RED BOLD

/// Level string for warning
const WARNING: &str = "\x1b[1;33m..WARNING..\x1b[0m"; // YELLOW BOLD

/// Level string for information
const INFORMATION: &str = "\x1b[1;37mINFORMATION\x1b[0m"; // WHITE BOLD

/// Level string for debug
const DEBUG: &str = "\x1b[1;34m...DEBUG...\x1b[0m"; // BLUE BOLD

/// Level string for trace
const TRACE: &str = "\x1b[1;32m...TRACE...\x1b[0m"; // GREEN BOLD

impl<W: Write + Send + Sync> Sink for Writer<W> {
    #[inline]
    fn record<S: Sink>(
        &self,
        event: &Event<'_, S>,
    ) {
        let level_string = match event.level {
            Level::Fatal => FATAL,
            Level::Error => ERROR,
            Level::Warning => WARNING,
            Level::Information => INFORMATION,
            Level::Debug => DEBUG,
            Level::Trace => TRACE,
        };

        self.0
            .lock()
            .expect("failed to lock writer")
            .write_fmt(format_args!(
                "{}{} {} {}{}:{}:{}:{}{} {}{}",
                CYAN,
                DateTime::now(),
                level_string,
                MAGENTA,
                event.metadata.module_path,
                event.metadata.file,
                event.metadata.line,
                event.metadata.column,
                RESET,
                event.message,
                "\n"
            ))
            .expect("failed to write");
        self.0
            .lock()
            .expect("failed to lock writer")
            .flush()
            .expect("failed to flush writer");
    }
}
