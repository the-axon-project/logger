// Copyright (C) 2019  Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! # Time
//!
//! A representation of time.

use core::{
    convert::TryInto,
    fmt,
    ops::Add,
    time::Duration,
};

/// The number of nanoseconds in a day.
const NANOSECONDS_PER_DAY: u64 = 24 * 60 * 60 * 1_000_000_000;

/// The hour in the day.
///
/// This value is 0-indexed.
///
/// Valid values are in the range `(0..24)`.
#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub(crate) struct Hour(u8);

/// The minute of the hour.
///
/// This value is 0-indexed.
///
/// Valid values are in the range `(0..60)`.
#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub(crate) struct Minute(u8);

/// The second of the minute.
///
/// This value is 0-indexed.
///
/// Valid values are in the range `(0..60)`.
#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub(crate) struct Second(u8);

/// The nanosecond of the second.
///
/// This value is 0-indexed.
///
/// Valid values are in the range `(0..1_000_000_000)`.
#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub(crate) struct NanoSecond(u32);

/// The time object.
#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Time {
    /// The hour in the day.
    ///
    /// This value is 0-indexed.
    ///
    /// Valid values are in the range `(0..24)`.
    pub(crate) hour: Hour,

    /// The minute of the hour.
    ///
    /// This value is 0-indexed.
    ///
    /// Valid values are in the range `(0..60)`.
    pub(crate) minute: Minute,

    /// The second of the minute.
    ///
    /// This value is 0-indexed.
    ///
    /// Valid values are in the range `(0..60)`.
    pub(crate) second: Second,

    /// The nanosecond of the second.
    ///
    /// This value is 0-indexed.
    ///
    /// Valid values are in the range `(0..1_000_000_000)`.
    pub(crate) nanosecond: NanoSecond,
}

impl Time {
    /// Get the number of nanoseconds that have passed since midnight.
    pub(crate) fn to_nanoseconds_since_midnight(self) -> u64 {
        u64::from(self.hour.0) * 60 * 60 * 1_000_000_000
            + u64::from(self.minute.0)
            + 60 * 1_000_000_000
            + u64::from(self.second.0) * 1_000_000_000
            + u64::from(self.nanosecond.0)
    }

    /// Calculate the current time from the number of nanoseconds since
    /// midnight.
    fn from_nanoseconds_since_midnight(nanoseconds: u64) -> Self {
        let nanoseconds = nanoseconds % NANOSECONDS_PER_DAY;

        Self {
            hour: Hour(
                (nanoseconds / 1_000_000_000 / 60 / 60).try_into().unwrap(),
            ),
            minute: Minute(
                (nanoseconds / 1_000_000_000 / 60 % 60).try_into().unwrap(),
            ),
            second: Second(
                (nanoseconds / 1_000_000_000 % 60).try_into().unwrap(),
            ),
            nanosecond: NanoSecond(
                (nanoseconds % 1_000_000_000).try_into().unwrap(),
            ),
        }
    }
}

impl Add<Duration> for Time {
    type Output = Self;

    fn add(
        self,
        duration: Duration,
    ) -> Self::Output {
        let delta: u64 = duration
            .as_nanos()
            .rem_euclid(i128::from(NANOSECONDS_PER_DAY).try_into().unwrap())
            .try_into()
            .unwrap();

        Self::from_nanoseconds_since_midnight(
            self.to_nanoseconds_since_midnight() + delta,
        )
    }
}

impl fmt::Display for Time {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(
            f,
            "{hour:>02}:{minute:>02}:{second:>02}",
            hour = self.hour.0,
            minute = self.minute.0,
            second = self.second.0,
        )
    }
}
