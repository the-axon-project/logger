// Copyright (C) 2019  Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! # Date
//!
//! A representation of date.

use core::{
    convert::TryInto,
    fmt,
    ops::{
        Add,
        Div,
    },
    time::Duration,
};

/// We only need 16 bits to represent a year.
/// We do not have any need to support years before UNIX EPOCH.
/// Hence, the valid values of `Year` are `(1970..DOOMSDAY_YEAR)`.
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub(crate) struct Year(u16);

impl Default for Year {
    fn default() -> Self {
        Self(1970)
    }
}

/// We only need 16 bits to represent the ordinal value.
/// The valid values of `Ordinal` are `(1..=366)`.
#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub(crate) struct Ordinal(u16);

impl Default for Ordinal {
    fn default() -> Self {
        Self(1)
    }
}

/// The year the world will come to an end.
///
/// We do not need to support years beyond the `DOOMSDAY_YEAR`.
/// The only reason we need this is to prevent the possibility of infinite
/// recursion in the statement `(1970..)`. The value of `DOOMSDAY_YEAR` can be
/// arbitrarily chosen.
///
/// ASSUMPTION: There is no year beyond `DOOMSDAY_YEAR`.
const DOOMSDAY_YEAR: Year = Year(2300);

/// The number of seconds in a day.
const SECONDS_PER_DAY: u32 = 24 * 60 * 60;

/// A date object.
///
/// The date is represented using the year along with the ordinal value.
#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct Date {
    /// The year the date represents.
    pub(crate) year: Year,

    /// The ordinal value representing the day of the year.
    ///
    /// This value is 1-indexed.
    pub(crate) ordinal: Ordinal,
}

impl Date {
    /// Check if the given date happens to be on a leap year.
    const fn is_leap_year(self) -> bool {
        (self.year.0 % 4 == 0)
            & ((self.year.0 % 100 != 0) | (self.year.0 % 400 == 0))
    }

    /// Get the number of days in the current year.
    fn days_in_year(self) -> u16 {
        if self.is_leap_year() {
            366
        } else {
            365
        }
    }

    /// Get the number of days in each month on the year.
    fn days_in_month(self) -> [u8; 12] {
        if self.is_leap_year() {
            [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        } else {
            [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
        }
    }

    /// Get the value of the current month representing the date.
    ///
    /// This value is 1-indexed.
    fn month(self) -> u8 {
        let month: usize = self
            .days_in_month()
            .iter()
            .scan(self.ordinal.0, |state, &x| {
                if *state < x.into() {
                    return None;
                }

                *state -= u16::from(x);

                Some(*state)
            })
            .count()
            + 1;

        let month: u8 = month.try_into().unwrap();

        month
    }

    /// Get the current day of the month.
    ///
    /// This value is 1-indexed.
    fn day(self) -> u8 {
        let day: u16 = self
            .days_in_month()
            .iter()
            .scan(self.ordinal.0, |state, &x| {
                if *state < x.into() {
                    return None;
                }

                *state -= u16::from(x);

                Some(*state)
            })
            .last()
            .unwrap_or(0)
            + 1;

        let day: u8 = day.try_into().unwrap();

        day
    }

    /// Calculate the number of days since UNIX EPOCH.
    ///
    /// This value is 0-indexed.
    fn to_days_since_epoch(self) -> u64 {
        (1970..DOOMSDAY_YEAR.0)
            .scan(u64::from(self.ordinal.0), |state, x| {
                // Create an arbitrary date. We only care about the year.
                let date = Self {
                    year: Year(x),
                    ordinal: Ordinal::default(),
                };

                if date.year < self.year {
                    *state += u64::from(date.days_in_year());

                    Some(*state)
                } else {
                    None
                }
            })
            .last()
            .unwrap_or_else(|| u64::from(self.ordinal.0))
            - 1
    }

    /// Create a date object by specifying the time passed since UNIX EPOCH.
    fn from_days_since_epoch(days: u64) -> Self {
        let iterator = (1970..DOOMSDAY_YEAR.0).scan(days, |state, x| {
            // Create an arbitrary date. We only care about the year.
            let date = Self {
                year: Year(x),
                ordinal: Ordinal::default(),
            };

            if *state >= u64::from(date.days_in_year()) {
                *state -= u64::from(date.days_in_year());

                Some(*state)
            } else {
                None
            }
        });

        let ordinal: Ordinal = Ordinal(
            iterator.clone().last().unwrap_or(days).try_into().unwrap(),
        );

        let year: Year = Year(iterator.count().try_into().unwrap());

        if ordinal.0 > 366 {
            panic!("DOOMSDAY!");
        }

        Self {
            year: Year(year.0 + 1970),
            ordinal: Ordinal(ordinal.0 + 1),
        }
    }
}

impl Add<Duration> for Date {
    type Output = Self;

    fn add(
        self,
        duration: Duration,
    ) -> Self::Output {
        let delta = duration.as_secs().div(u64::from(SECONDS_PER_DAY));

        let delta = delta.checked_sub(1).unwrap_or(delta);

        Self::from_days_since_epoch(self.to_days_since_epoch() + delta)
    }
}

impl fmt::Display for Date {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(f, "{}-{}-{}", self.year.0, self.month(), self.day())
    }
}

#[test]
fn leap_year() {
    assert!(
        (1970..DOOMSDAY_YEAR.0)
            .filter(|x| x % 4 == 0)
            .filter(|x| x % 100 != 0 || x % 400 == 0)
            .all(|x| {
                let date = Date {
                    year: Year(x),
                    ordinal: Ordinal::default(),
                };

                date.is_leap_year()
            })
    )
}

#[test]
fn month() {
    let leap = (1..365).all(|x| {
        let date = Date {
            year: Year(1972),
            ordinal: Ordinal(x),
        };

        date.month() >= 1 && date.month() <= 12
    });

    let epoch = (1..365).all(|x| {
        let date = Date {
            year: Year::default(),
            ordinal: Ordinal(x),
        };

        date.month() >= 1 && date.month() <= 12
    });

    assert!(leap && epoch);
}

#[test]
fn day() {
    let leap = (1..365).all(|x| {
        let date = Date {
            year: Year(1972),
            ordinal: Ordinal(x),
        };

        date.day() >= 1 && date.day() <= 31
    });

    let epoch = (1..365).all(|x| {
        let date = Date {
            year: Year::default(),
            ordinal: Ordinal(x),
        };

        date.day() >= 1 && date.day() <= 31
    });

    assert!(leap && epoch);
}

#[test]
fn idempotence() {
    let maxdays: u64 = (u64::from(DOOMSDAY_YEAR.0) - 1970) * 365;

    assert!(
        (0..maxdays).all(|x| {
            Date::from_days_since_epoch(x).to_days_since_epoch() == x
        })
    )
}

#[test]
fn conversion_validity() {
    let maxdays: u64 = (u64::from(DOOMSDAY_YEAR.0) - 1970) * 365;

    assert!((0..maxdays).all(|x| {
        let date = Date::from_days_since_epoch(x);

        date.year.0 >= 1970
            && date.year <= DOOMSDAY_YEAR
            && date.ordinal.0 >= 1
            && date.ordinal.0 <= 366
    }))
}
