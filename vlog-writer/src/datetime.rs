// Copyright (C) 2019  Andrew Green <tuxsoy@protonmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! # Date and Time
//!
//! A representation for date and time.

use crate::{
    date::Date,
    time::Time,
};
use core::{
    convert::{
        TryFrom,
        TryInto,
    },
    fmt,
    ops::{
        Add,
        Rem,
    },
    time::Duration,
};
use std::time::SystemTime;

/// The number of seconds in a day.
const SECONDS_PER_DAY: u32 = 24 * 60 * 60;

/// The number of nanoseconds in a day.
const NANOSECONDS_PER_DAY: u64 = 24 * 60 * 60 * 1_000_000_000;

/// The datetime object.
#[derive(Debug, Copy, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct DateTime {
    /// Represents the current date.
    pub(crate) date: Date,

    /// Represents the current time.
    pub(crate) time: Time,
}

impl DateTime {
    /// Get the system time.
    pub(crate) fn now() -> Self {
        SystemTime::now().try_into().unwrap()
    }
}

impl Add<Duration> for DateTime {
    type Output = Self;

    fn add(
        self,
        duration: Duration,
    ) -> Self::Output {
        let delta_nanoseconds_since_midnight: u64 =
            (duration.as_nanos().rem(u128::from(NANOSECONDS_PER_DAY)))
                .try_into()
                .unwrap();

        let nanoseconds: u64 = self.time.to_nanoseconds_since_midnight()
            + delta_nanoseconds_since_midnight;

        let delta = if nanoseconds >= NANOSECONDS_PER_DAY {
            Duration::from_secs(u64::from(SECONDS_PER_DAY))
        } else {
            Duration::from_secs(0)
        };

        Self {
            date: self.date + duration + delta,
            time: self.time + duration,
        }
    }
}

impl TryFrom<SystemTime> for DateTime {
    type Error = &'static str;

    fn try_from(system_time: SystemTime) -> Result<Self, Self::Error> {
        match system_time.duration_since(SystemTime::UNIX_EPOCH) {
            Ok(duration) => Ok(Self::default() + duration),
            Err(_) => Err("time before UNIX_EPOCH causes undefined behavior"),
        }
    }
}

impl fmt::Display for DateTime {
    fn fmt(
        &self,
        f: &mut fmt::Formatter<'_>,
    ) -> fmt::Result {
        write!(f, "{}T{}Z", self.date, self.time)
    }
}
